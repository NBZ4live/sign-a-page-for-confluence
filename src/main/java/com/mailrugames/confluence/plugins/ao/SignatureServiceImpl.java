package com.mailrugames.confluence.plugins.ao;

import com.atlassian.activeobjects.external.ActiveObjects;

import java.sql.Timestamp;
import java.util.List;

import net.java.ao.Query;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;

public class SignatureServiceImpl implements SignatureService {
	
	private final ActiveObjects ao;
	
	public SignatureServiceImpl(ActiveObjects ao) {
		this.ao = checkNotNull(ao);
	}

	@Override
	public Signature add(String signer, long pageId, int version) {
		// Get sure that the entity is unique.
		Signature signature = this.getSignature(signer, pageId, version);
		if (signature != null) {
			return signature;
		}
		else {
			signature = ao.create(Signature.class);
			signature.setSigner(signer);
			signature.setPageId(pageId);
			signature.setVersion(version);
			
			java.util.Date date = new java.util.Date();
			signature.setSignTime(new Timestamp(date.getTime()));
			
			signature.save();
			
			return signature;
		}
	}

	@Override
	public List<Signature> all() {
		return newArrayList(ao.find(Signature.class));
	}
	
	@Override
	public Signature getSignature(String signer, long pageId, int version) {
		List<Signature> signature = newArrayList(ao.find(Signature.class, Query.select().where("signer = ? AND page_id = ? AND version = ?", signer, pageId, version)));
		
		if (signature.isEmpty()) {
			return null;
		}
		else {
			return signature.get(0);
		}
	}

	@Override
	public List<Signature> getSignatures(long pageId) {
		return newArrayList(ao.find(Signature.class, Query.select().where("page_id = ?", pageId)));
	}

	@Override
	public List<Signature> getSignatures(String signer, long pageId) {
		return newArrayList(ao.find(Signature.class, Query.select().where("signer = ? AND page_id = ?", signer, pageId)));
	}

}
