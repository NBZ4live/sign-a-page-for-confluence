package com.mailrugames.confluence.plugins.ao;

import com.mailrugames.confluence.plugins.ao.Signature;
import com.atlassian.activeobjects.tx.Transactional;

import java.util.List;

@Transactional
public interface SignatureService {
	Signature add(String signer, long pageId, int version);
	
	List<Signature> all();
	
	Signature getSignature(String signer, long pageId, int version);
	
	List<Signature> getSignatures(long pageId);
	List<Signature> getSignatures(String signer, long pageId);
}
