package com.mailrugames.confluence.plugins.ao;

import net.java.ao.Entity;
import net.java.ao.Preload;
import java.sql.Timestamp;

@Preload
public interface Signature extends Entity {
	String getSigner();
	
	void setSigner(String signer);
	
	long getPageId();
	
	void setPageId(long pageId);
	
	int getVersion();
	
	void setVersion(int version);
	
	Timestamp getSignTime();
	
	void setSignTime(Timestamp signTime);
}
