package com.mailrugames.confluence.plugins.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
//import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;
import com.atlassian.confluence.security.ContentPermission;
import com.atlassian.confluence.security.ContentPermissionSet;
import com.mailrugames.confluence.plugins.ao.Signature;
import com.mailrugames.confluence.plugins.ao.SignatureService;
import com.opensymphony.util.TextUtils;
import com.opensymphony.webwork.ServletActionContext;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import static com.google.common.base.Preconditions.checkNotNull;

public class SignMacro extends BaseMacro implements Macro {

	protected final PageManager pageManager;
	protected final UserAccessor userAccessor;
	protected final SignatureService signatureService;
	
	protected long pageId;
	protected int version;
	protected boolean isLatestVersion;
	
	public SignMacro(PageManager pageManager, UserAccessor userAccessor, SignatureService signatureService) {
		this.pageManager = pageManager;
		this.userAccessor = userAccessor;
		this.signatureService = checkNotNull(signatureService);
	}
	
	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}

	@Override
	public RenderMode getBodyRenderMode() {
		return RenderMode.NO_RENDER;
	}

	@Override
	public boolean hasBody() {
		return true;
	}
	
	@Override
	public boolean isInline() {
		return false;
	}

	@Override
	public String execute(Map<String, String> parameters, String body,
			ConversionContext context) throws MacroExecutionException {
		
		try {
            return execute(parameters, body, (RenderContext) context.getPageContext());
        } catch (MacroException e) {
            throw new MacroExecutionException(e);
        }
	}

	@Override
	public String execute(@SuppressWarnings("rawtypes")Map parameters, String body,
			RenderContext renderContext) throws MacroException {
		
		ContentEntityObject contentObject = ((PageContext) renderContext).getEntity();
		
		this.pageId = contentObject.getId();
		this.version = contentObject.getLatestVersion().getVersion();
		this.isLatestVersion = contentObject.isLatestVersion();
		
		HttpServletRequest request = ServletActionContext.getRequest();
        String remoteUser = null;
        if (request != null) {
            remoteUser = request.getRemoteUser();
            saveSignature(request);
        }
        
        boolean canViewSignatures = canViewSignatures(remoteUser, contentObject);
        
        Map<String, Signature> allPageSignaturesMap = null;
        if (canViewSignatures) {
        	allPageSignaturesMap = getAllPageSignatures();
        }
        
		
		Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();
		
		contextMap.put("content", contentObject);
		contextMap.put("title", (String) parameters.get("title"));
		contextMap.put("message", (String) parameters.get("message"));
		contextMap.put("buttonText", (String) parameters.get("buttonText"));
		contextMap.put("canSign", canSign(remoteUser));
		contextMap.put("canViewSignatures", canViewSignatures);
		contextMap.put("pageId", this.pageId);
		contextMap.put("version", this.version);
		contextMap.put("isLatestVersion", this.isLatestVersion);
		contextMap.put("remoteUser", remoteUser);
		contextMap.put("allPageSignaturesMap", allPageSignaturesMap);
		
		try {
            return VelocityUtils.getRenderedTemplate("templates/signmacro.vm", contextMap);
        } catch (Exception e) {
            throw new MacroException(e);
        }
	}
	
	protected Map<String, Signature> getAllPageSignatures() {
		Map<String, Signature> allPageSignaturesMap = new HashMap<String, Signature>();
		List<Signature> allPageSignaturesList = signatureService.getSignatures(this.pageId);
		
		if (allPageSignaturesList.isEmpty()) {
			return null;
		}
		
		for (Signature signature : allPageSignaturesList) {
			if (allPageSignaturesMap.containsKey(signature.getSigner())) {
				if (signature.getVersion() > allPageSignaturesMap.get(signature.getSigner()).getVersion()) {
					allPageSignaturesMap.put(signature.getSigner(), signature);
				}
			}
			else {
				allPageSignaturesMap.put(signature.getSigner(), signature);
			}
		}
		
		return allPageSignaturesMap;
	}
	
	protected void saveSignature(HttpServletRequest request) {
		String signer = request.getRemoteUser();
		
		if (request.getParameter("sign") != null && canSign(signer)) {
			signatureService.add(signer, pageId, version);
		}
	}
	
	protected Boolean canSign(String signer) {
		Signature signature = signatureService.getSignature(signer, this.pageId, this.version);
		
		return signature == null ? true : false;
	}
	
	protected Boolean canViewSignatures(String username, ContentEntityObject contentObject) {
		if (!TextUtils.stringSet(username)) {
			return false;
		}
		
		User user = userAccessor.getUser(username);
		
		if (contentObject.hasPermissions(ContentPermission.EDIT_PERMISSION)) {
			ContentPermissionSet contentPermissionSet = contentObject.getContentPermissionSet(ContentPermission.EDIT_PERMISSION);
			if (contentPermissionSet.isPermitted(user)) {
				return true;
			}
		}
		else if (user.getName().equals(contentObject.getCreatorName())) {
			return true;
		}
		
		return false;
	}

}
